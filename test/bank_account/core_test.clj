(ns bank-account.core-test
  (:require [bank-account.core :as core]
            [midje.sweet :refer :all]))

(facts "when parsing command-line args"
  (fact "creates an accounts map if parseable"
    @(core/parse-args ["0001=10" "0002=20.5"]) => {"0001" 10M, "0002" 20.5M})

  (fact "fails if accounts map isn't on the format we expect"
    (core/parse-args ["001=10" "bar"]) => nil)

  (fact "fails if amount part isn't a number"
    (core/parse-args ["001=10" "bar=foo"]) => nil))
