(ns bank-account.accounts-test
  (:require [bank-account.accounts :as accounts]
            [midje.sweet :refer :all]))

(facts "when transfering money from accounts"
  (let [accounts (atom {"1" 10M, "2" 4M})]
    (tabular
      (fact "transfers values from accounts"
        (accounts/transfer! accounts ?from ?to ?amount) => ?result
        @accounts => ?accounts)
      ?from  ?to  ?amount  ?result                           ?accounts
      "1"    "2"  5M       {:balance 5M}                     {"1" 5M, "2" 9M}
      "9"    "2"  5M       {:errors [:invalid-origin]}       @accounts
      "1"    "9"  5M       {:errors [:invalid-destination]}  @accounts
      "1"    "2"  15M      {:errors [:negative-balance]}     @accounts
      "1"    "2"  -1M      {:errors [:negative-amount]}      @accounts)))
