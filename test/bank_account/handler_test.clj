(ns bank-account.handler-test
  (:require [cheshire.core :as json]
            [midje.sweet :refer :all]
            [ring.mock.request :as mock]
            [bank-account.handler :as handler]))

(defn make-handler []
  (handler/app-routes (atom {"1" 10M "2" 7M})))

(defn make-request [handler method path payload]
  (let [req (-> (mock/request method path)
                (mock/body (json/generate-string payload))
                (mock/header "Content-Type" "application/json"))
        response (handler req)]
    (assoc response
           :json-body (-> response :body json/decode))))

(fact "will return 404 if route doesn't exist"
  (let [response (make-request (make-handler)
                               :get "/strange/route" {})]
    (:status response) => 404
    (:body response) => "{\"error\":\"Not found\"}"))

(facts "when getting account balance"
  (fact "will return balance when account exists"
    (let [response (make-request (make-handler)
                                 :get "/accounts/1" {})]
      (:body response) => "{\"balance\":10}"))

  (fact "will return 404 if account doesn't exist"
    (let [response (make-request (make-handler)
                                 :get "/accounts/10" {})]
      (:status response) => 404
      (:body response) => "{\"error\":\"Not found\"}")))

(facts "when transfering money"
  (let [handler (make-handler)]
    (fact "will return an error if an invalid option was sent"
      (let [response (make-request handler :post "/accounts/1"
                                   {:transfer {:account "2" :amount 12}})]
        (:json-body response) => {"errors" ["negative-balance"]}
        (:status response) => 422))

    (fact "will return an error if payload is invalid"
      (let [response (make-request handler :post "/accounts/1"
                                   {:transfer {:account "2" :amount "foo"}})]
        (:json-body response) => {"errors" ["invalid payload"]}
        (:status response) => 422))

    (fact "will transfer amount"
      (let [response (make-request handler :post "/accounts/1"
                                   {:transfer {:account "2" :amount 7}})]
        (:json-body response) => {"balance" 3}
        (:status response) => 200))

    (fact "will keep state"
      (let [response (make-request handler :post "/accounts/1"
                                   {:transfer {:account "2" :amount 1}})]
        (:json-body response) => {"balance" 2}))))
