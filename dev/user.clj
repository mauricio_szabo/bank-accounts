(ns user
  (:require [bank-account.core :as core]))

(def server (atom nil))

(defn stop! []
  (some-> server deref .stop))

(defn start! []
  (reset! server (core/-main)))
