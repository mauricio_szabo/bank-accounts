(ns bank-account.core
  (:gen-class)
  (:require [ring.adapter.jetty :as jetty]
            [bank-account.handler :as handler]
            [clojure.string :as str]))

(defn start-server! [accounts]
  (let [handler (handler/app-routes accounts)]
    (jetty/run-jetty handler {:port 3000
                              :join? false})))

(defn- separate-acc-and-amount [account-elem]
  (let [[account amount] (str/split account-elem #"=")]
    (cond
      (re-matches #"\d+(\.\d+)?" (str amount)) [account (bigdec amount)]
      :else [:error :error])))

(defn parse-args [args]
  (let [accounts-map (->> args
                         (map separate-acc-and-amount)
                         (into {}))]
    (when-not (:error accounts-map)
      (atom accounts-map))))

(defn -main [ & args]
  (let [options (if (empty? args)
                  (str/split (str (System/getenv "ACCOUNTS")) #"\s+")
                  args)]
    (if-let [accounts (parse-args options)]
      (start-server! accounts)
      (println "Usage: lein run <accounts>

Where <accounts> is a pair of <account>=<amount>, repeated as many times as you want.
For example, to start a server with accounts 001 and 002, both with BRL 10,
we can use:

lein run 001=10 002=10"))))
