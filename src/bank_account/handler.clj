(ns bank-account.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :as defaults]
            [ring.middleware.json :as json]
            [bank-account.accounts :as accounts]))

(def not-found {:status 404
                :body {:error "Not found"}})

(defn- get-balance [accounts id]
  (if-let [balance (get @accounts id)]
    {:status 200 :body {:balance balance}}
    not-found))

(defn- do-transfer [accounts from to amount]
  (let [result (accounts/transfer! accounts from to (bigdec amount))]
    {:status (if (:errors result) 422 200)
     :body result}))


(defn- transfer [accounts {:keys [id transfer]}]
  (let [{:keys [amount account]} transfer]
    (if (and amount account id (re-matches #"\d+(\.\d+)?" (str amount)))
      (do-transfer accounts id account amount)
      {:status 422 :body {:errors ["invalid payload"]}})))

(defn app-routes [accounts]
  (->
    (routes
      (GET "/" [] "Hello There")
      (GET "/accounts/:id" [ & {:keys [id]}] (get-balance accounts id))
      (POST "/accounts/:id" [ & params] (transfer accounts params))
      (route/not-found not-found))
    (defaults/wrap-defaults defaults/api-defaults)
    json/wrap-json-response
    json/wrap-json-params))
