(ns bank-account.accounts)

(defn- make-transfer [accounts from to amount]
  (let [new-val-on-origin (some-> accounts (get from) (- amount))
        new-val-on-dest (some-> accounts (get to) (+ amount))
        errors (cond-> []
                (nil? new-val-on-origin) (conj :invalid-origin)
                (nil? new-val-on-dest) (conj :invalid-destination)
                (and new-val-on-origin (< new-val-on-origin 0)) (conj :negative-balance)
                (< amount 0) (conj :negative-amount))]
    (if (empty? errors)
      (with-meta (assoc accounts from new-val-on-origin to new-val-on-dest) nil)
      (with-meta accounts {:errors errors}))))

(defn transfer! [accounts-atom from to amount]
  (let [new-values (swap! accounts-atom make-transfer from to amount)]
    (if-let [errors (meta new-values)]
      errors
      {:balance (get new-values from)})))
