# Bank Account

This application simulates a real bank where you can show your account balance, and
transfer your money to another account.

To boot up a web server, run:

```
lein run <accounts>
```

Where `<accounts>` is a list of accounts, separated by spaces, in the format
`<account-identifier>=<amount>`. So, if you want three accounts on your application, you
can use:

```
lein run 0001=10000 0002=100 0003=9100
```

It'll also accept an environment variable, `ACCOUNTS`, in the same format.

## Developing

First, install [Leiningen][]. The instructions for installation are on the official site.
Then, set environment variable `ACCOUNTS` to anything - this step is not necessary, but'll
help you if you want to test something while developing.

`lein repl` will boot up a REPL. You can connect your editor to the REPL using any plug-in
you want: Emacs with Cider, Atom with Proto-REPL, vim with Fireplace.

All account info is inside an `atom`, containing a map where keys are the account numbers
and values are amounts. All mutation of this atom occurs on a single function -
`accounts/make-transfer` - because it needs to be atomic otherwise we can have concurrent
updates.

Error checking is being done by setting a metadata on the account map so we know what
error has occurred. The metadata must be cleared if a successful update has occurred.

This project supports [Clojure
Workflows](http://thinkrelevance.com/blog/2013/06/04/clojure-workflow-reloaded) defined by
people of `tools.namespace`. This means that all code is refreshable at any time - we need
just to call `(user/stop!)` to stop the webserver, then `(user/start!)` after re-loading
all the code.

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed. Clojure or other libraries are not
required - they will be downloaded when needed.

[leiningen]: https://github.com/technomancy/leiningen
